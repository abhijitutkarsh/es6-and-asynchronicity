const posts = [{ title: 'Post one' , body:'This is post one' }];
function createPost(post,callback)
{
    setTimeout((post)=>
    {
        posts.push(post);
        return callback(posts);
    },2000);
}
function getPost(arr)
{
    setTimeout((arr)=>
    {
        arr.foreach(function(i)
        {
            console.log(i);
        });

    },1000);
}
module.exports = {
  posts: posts,
  createPost: createPost,
  getPost: getPost
};

