function filter5(array)
{
    var k=[];
    function check(ele)
    {
        if(ele!=5)
            return(ele);
    }
    k=array.filter(check);
    return(k);
}
function filter10(array)
{
    var k=[];
    function check(ele)
    {
        if(ele!=10)
            return(ele);
    }
    k=array.filter(check);
    return(k);
}
function filter(array,callback)
{
    return callback(array);
}
module.exports = {
 ten: filter10,
 five: filter5,
 fil: filter
};
