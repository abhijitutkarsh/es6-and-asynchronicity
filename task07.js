function fizz(array)
{
    var count=0;
    array.forEach((ele)=>
    {
        if(ele%3===0)
            count+=1;
    }
    );
    return(count);
}
function buzz(array)
{
    var count=0;
    array.forEach((ele)=>
    {
        if(ele%5===0)
            count+=1;
    }
    );
    return(count);
}
function filter(array,callback)
{
    return callback(array);
}
module.exports = {
 fizz: fizz,
 buzz: buzz,
 fil: filter
};
