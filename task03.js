function division(num1,num2,callback)
{
    if(num2!=0)
    {
        var result1=num1/num2;
        return callback("no error",result1);
    }
    else{
        return callback("num2 shouldn't be zero",null);
    }
}

function callback(err,result)
{
    if(result===null)
        return(err);
    else
    {
        return(result);
    }
}
module.exports = {
 div: division,
 call: callback
};

