var fs = require("fs");
function read(file) {
 return new Promise((resolve, reject) => {
 fs.readFile(file, "utf8", (err, data) => {
 if (err) {
 reject(err);
 } else {
 resolve(data);
 }
 });
 });
}
async function count(path)
{
    var c=await read(path);
    return (c.split(" ").length);
}
module.exports = {
  counter: count
};

