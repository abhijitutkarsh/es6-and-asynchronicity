module.exports = {
 created: create
};
var fs=require("fs");
function func(x)
{
    return new Promise((resolve,reject)=>
    {
        fs.writeFile("file1.txt",x,(err,data)=>
        {
            if(!err)
            resolve("file created");
            else
            reject(err);

        });

    });
}
async function create(x)
{
    var c=await func(x);
    return c;
}
