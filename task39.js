const fetch = require("node-fetch");
const generateNumbers = async function* (){
	let url = 'https://www.random.org/integers/?num=1&min=1&max=9&col=1&base=10&format=plain&rnd=new';
	yield await fetch(url).then(result => result.json()) * 10;	
}

module.exports = {
	asyncRandomNumbers: generateNumbers 
}


